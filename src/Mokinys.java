public class Mokinys extends Zmogus {
    static public double mokiniuVidurkis = 0;


    private int[] pazymiai = new int[100];
    private int pazymiuKiekis = 0;
    public double pazymiuVidurkis = 0;
    private String klase = "5B";


    Mokinys(String vardas, String pavarde) {
        super(vardas, pavarde);
    }

    public static void skaiciuotiMokiniuVidurki(Mokinys[] zmones) {
        double visasVidurkis = 0;
        double sumaVisa = 0;

        for(int i = 0; i < zmones.length; i ++) {
            sumaVisa +=   zmones[i].pazymiuVidurkis;
        }

        mokiniuVidurkis = sumaVisa / (double)zmones.length;
    }

    public void pridetiPazymi(int pazymys) {
        if(pazymys > 0 && pazymys <= 10) {
            this.pazymiai[this.pazymiuKiekis] = pazymys;
            this.pazymiuKiekis++;

            this.skaiciuotiVidurki();

            System.out.println("Mokinui " + this.vardasPavarde() + " Sekmingai pridetas pazymys: " + pazymys);
        }

        mokiniuVidurkis = 55;

    }


    private void skaiciuotiVidurki() {
        int suma = 0;
        double vidurkis = 0;
        for(int i = 0; i < this.pazymiuKiekis; i++) {
            suma += this.pazymiai[i];
        }

        vidurkis = (double)suma / (double)this.pazymiuKiekis;

        this.pazymiuVidurkis = vidurkis;
    }

    public void spausdintiVardaSuKlase() {
        System.out.println(this.vardasPavarde() + " klase: " + this.klase);
    }
}
