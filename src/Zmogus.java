public class Zmogus {

    private String vardas = "aaaaa";
    private String pavarde; //priskiriame pradine reiksme



    private int amzius = 10;

    Zmogus(String vardas, String pavarde) {
        this.vardas = vardas;
        this.pavarde = pavarde;

        System.out.println("Sukurtas naujas zmmogaus objektas");
    }

    public int getAmzius() {
        return this.amzius;
    }

    public void setAmzius(int amzius) {
        if(this.amzius > 0) {
            this.amzius = amzius;
        }
    }

    public String getPavarde() {
        if(this.pavarde == null) {
            return "";
        } else {
            return this.pavarde;
        }
    }


    public String vardasPavarde() {

        if(this.pavarde == null) {
            this.pavarde = "";
        }

        return this.vardas + " " + this.pavarde;
    }

    public String amziausGrupe() {
        if(this.amzius < 18) {
            return "Tu esi vaikas";
        } else if(this.amzius > 18 && this.amzius < 67) {
            return "Tu esi pilnametis";
        } else {
            return "Tu esi pensininkas";
        }
    }
}