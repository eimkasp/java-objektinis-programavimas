import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        // Sukuriame zmogaus klases objektu masyva
        Mokinys[] zmones = new Mokinys[3];

        // kintamasis gauti vartotojo ivestiems duomenims
        Scanner input = new Scanner(System.in);
        String inputVardas;
        String inputPavarde;

        zmones[0] = new Mokinys("mokinys 1", "pavarde");
        zmones[0].pridetiPazymi(1);
        zmones[0].pridetiPazymi(6);
        zmones[0].pridetiPazymi(8);

        zmones[0].getAmzius(); // 10


        zmones[0].setAmzius(55);

        zmones[0].getAmzius();




        zmones[1] = new Mokinys("mokinys 2", "pavarde");
        zmones[1].pridetiPazymi(8);
        zmones[1].pridetiPazymi(5);
        zmones[1].pridetiPazymi(8);


        zmones[2] = new Mokinys("mokinys 3", "pavarde");
        zmones[2].pridetiPazymi(9);
        zmones[2].pridetiPazymi(10);



        geriausiasMokinys(zmones);

        Mokinys.skaiciuotiMokiniuVidurki(zmones);

    }

    public static void spausdintiZmones(Mokinys[] zmones) {
        for(int i = 0; i < zmones.length; i++ ) {
            System.out.println("Zmogus nr " + i+1 + " " + zmones[i].vardasPavarde());
        }
    }

    public static void geriausiasMokinys(Mokinys[] zmones) {
        /* Einame per visus mokinius */
        double max = Double.MIN_VALUE; // maziausia imanoma double tipo kintamojo reiksme
        int geriausiasMokinysIndex = 0;

        for(int i = 0; i < zmones.length; i++ ) {
            if(zmones[i].pazymiuVidurkis > max) {
                max = zmones[i].pazymiuVidurkis;
                geriausiasMokinysIndex = i;
            }
        }

        /* Reikia atspausdinti mokiny su geriausiu vidurkiu */
        System.out.println("Didiausias vidurkis: " + max );
        System.out.println("Geriausias mokinys: " + zmones[geriausiasMokinysIndex].vardasPavarde() );
    }
}
